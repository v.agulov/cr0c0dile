package com.korostelev.cr0c0dile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.korostelev.cr0c0dile.adapters.MessageAdapter;
import com.korostelev.cr0c0dile.drawing.DrawingView;
import com.korostelev.cr0c0dile.adapters.Message;
import com.korostelev.cr0c0dile.model.ConnectionStatus;
import com.korostelev.cr0c0dile.model.GameMessage;
import com.korostelev.cr0c0dile.model.User;
import com.korostelev.cr0c0dile.network.GameSession;
import com.korostelev.cr0c0dile.network.StompConnection;
import com.turkialkhateeb.materialcolorpicker.ColorChooserDialog;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class DrawingActivity extends AppCompatActivity {

    private static final String TAG = "Drawing Activity";
    private StompConnection stompConnection;
    private GameSession gameSession;

    private EditText chatInput;
    private Button sendBtn;


    private DrawingView canvas;
    private Button colorPicker;
    private Button brushButton;
    private Button resetBtn;
    private Button exitButton;

    private TextView gameStatusText;
    private TextView gameInfoText;

    RecyclerView chatView;
    MessageAdapter messageAdapter;
    List<Message> messages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing);
        initView();

        gameSession = (GameSession)getApplicationContext();
        gameSession.updateDrawingActivity(DrawingActivity.this);
        stompConnection = gameSession.getStompConnection();

        setGameRole();
    }

    private void initView(){

        canvas = findViewById(R.id.drawing_view);
        canvas.setDrawingActivity(DrawingActivity.this);


        chatInput = findViewById(R.id.chat_input);
        sendBtn  = findViewById(R.id.btn_send);



        colorPicker = findViewById(R.id.color_picker_btn);
        brushButton = findViewById(R.id.brush_button);
        resetBtn = findViewById(R.id.reset_btn);
        exitButton = findViewById(R.id.exit_button);


        gameStatusText = findViewById(R.id.game_status);
        gameInfoText = findViewById(R.id.game_info);

        chatView = findViewById(R.id.msg_list_view);
        setupControlEvents();

        chatView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        manager.setStackFromEnd(true);
        chatView.setLayoutManager(manager);

        messages = new ArrayList<>();
        messageAdapter = new MessageAdapter(DrawingActivity.this, messages);
        chatView.setAdapter(messageAdapter);
    }






    public void updateCanvas(GameMessage msg){
        canvas.drawLine(msg);
    }

    public void sendGameMessage(GameMessage msg){
        stompConnection.sendGameMessageOnDraw(msg);
    }

    public void sendChatMessage(GameMessage msg){stompConnection.sendChatMessage(msg); }

    public void updateChat(GameMessage msg){updateChatView(msg.getGuesserName(), msg.getWord()); }


    @SuppressLint("SetTextI18n")
    public void setGameRole(){
        User user = gameSession.getUser();

        ConnectionStatus status = user.getStatus();
        if(status == ConnectionStatus.MASTER){
            String wordInfo = String.format("%s %s", getString(R.string.game_master_status), user.getWord());
            gameStatusText.setText(wordInfo);
            sendBtn.setEnabled(false);
            canvas.setActive(true);
            resetBtn.setEnabled(true);
        }else{
            gameStatusText.setText(getString(R.string.game_guesser_status));
            canvas.setActive(false);
            sendBtn.setEnabled(true);
            resetBtn.setEnabled(false);
        }
        String infoText = String.format("%s %s    %s %s", getString(R.string.game_info_room), user.getRoom()
                , getString(R.string.game_info_user), user.getName());

        gameInfoText.setText(infoText);
        canvas.resetCanvas();
    }


    private void updateChatView(String sender, String chatMsg){
        messages.add(new Message(sender, chatMsg));
        messageAdapter = new MessageAdapter(DrawingActivity.this, messages);
        chatView.setAdapter(messageAdapter);
        chatInput.setText("");
    }

    private void setupControlEvents(){
        colorPicker.setOnClickListener(v -> {

            ColorChooserDialog dialog = new ColorChooserDialog(DrawingActivity.this);
            dialog.setTitle("Select Color");
            dialog.setColorListener((v1, color) -> canvas.setDrawColor(color));
            dialog.show();
        });


        exitButton.setOnClickListener(v -> new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to leave?")
                .setPositiveButton("Yes", (dialog, which) -> finish())
                .setNegativeButton("No", null)
                .show());


        sendBtn.setOnClickListener(v -> {
            try {
                InputMethodManager m = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (m != null) {
                    m.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                }
            }catch (Exception e){
                Log.e(TAG, Objects.requireNonNull(e.getMessage()));
            }

            String chatMsg = chatInput.getText().toString();

            if(chatMsg.trim().equals("")){
                return;
            }
            updateChatView(gameSession.getUser().getName(), chatMsg);
            DrawingActivity.this.sendChatMessage(new GameMessage(chatMsg));
            chatInput.setText("");
        });

        resetBtn.setOnClickListener(v -> canvas.clearCanvas());
        brushButton.setOnClickListener(v -> createBrushWidthDialog());

    }


    private void createBrushWidthDialog(){
        final AlertDialog.Builder brushDialog = new AlertDialog.Builder(this);
        final SeekBar seek = new SeekBar(this);
        seek.setMax(40);
        seek.setKeyProgressIncrement(1);

        brushDialog.setPositiveButtonIcon(getResources().getDrawable(R.drawable.ic_check_black_24dp ));
        brushDialog.setIcon(R.drawable.ic_brush_black_24dp);
        brushDialog.setTitle("Select brush width");
        brushDialog.setView(seek);

        brushDialog.setPositiveButton("", (dialog, id) -> {
            int progress = seek.getProgress();
            if(progress == 0) return;
            canvas.setLineWidth(progress + DrawingView.MIN_STROKE_WIDTH);
        });
        brushDialog.show();
    }
}
