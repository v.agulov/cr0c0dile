package com.korostelev.cr0c0dile;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.korostelev.cr0c0dile.network.GameSession;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends AppCompatActivity {


    private Button loginButton;
    private MaterialEditText userName, roomName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.login);
        userName = findViewById(R.id.user_name);
        roomName = findViewById(R.id.room_name);
        initView();
    }

    public void startDrawingActivity(){
        Intent intent = new Intent(LoginActivity.this , DrawingActivity.class);
        startActivity(intent);
    }


    private void initView(){
        final Animation buttonAnim = AnimationUtils.loadAnimation(this, R.anim.button_press);
        loginButton.setAnimation(buttonAnim);

        loginButton.setOnClickListener(v -> {

            v.startAnimation(buttonAnim);
            Editable name = userName.getText();
            Editable room = roomName.getText();

            if(name == null || room == null){
                Toast.makeText(LoginActivity.this, "All fields are required.", Toast.LENGTH_SHORT).show();
                return;
            }
            if(name.toString().equals("") || room.toString().equals("")){
                Toast.makeText(LoginActivity.this, "All fields are required.", Toast.LENGTH_SHORT).show();
                return;
            }

            findViewById(R.id.loading_panel).setVisibility(View.VISIBLE);
            GameSession session = (GameSession)getApplicationContext();
            session.restart();
            session.getStompConnection().connect(
                    name.toString(), room.toString(), LoginActivity.this
            );
        });

    }
}
