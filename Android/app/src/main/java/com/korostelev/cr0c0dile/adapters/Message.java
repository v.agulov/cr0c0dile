package com.korostelev.cr0c0dile.adapters;

public class Message {
    private String sender;
    private String msg;

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    String getSender() {
        return sender;
    }

    String getMsg() {
        return msg;
    }

    public Message(String sender, String msg) {
        this.sender = sender;
        this.msg = msg;
    }
}
