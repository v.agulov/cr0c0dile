package com.korostelev.cr0c0dile.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.korostelev.cr0c0dile.R;


import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    private Context mContext;
    private List<Message> mChat;


    public MessageAdapter(Context mContext, List<Message> mChat) {
        this.mContext = mContext;
        this.mChat = mChat;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chat_message, viewGroup, false);
        return new MessageViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int i) {
        Message msg = mChat.get(i);
        messageViewHolder.userName.setText(msg.getSender() + ": ");
        messageViewHolder.message.setText(msg.getMsg());
    }

    @Override
    public int getItemCount() {
        return mChat.size();
    }

    static class MessageViewHolder extends RecyclerView.ViewHolder{

        TextView message;
        TextView userName;


        MessageViewHolder(View itemView){
            super(itemView);
            message = itemView.findViewById(R.id.user_message);
            userName = itemView.findViewById(R.id.user_name);
        }
    }
}
