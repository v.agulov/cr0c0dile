package com.korostelev.cr0c0dile.drawing;

import android.graphics.Color;

public class DrawingUtils {

    private DrawingUtils(){}

    static int normCoordinate(float v, float width){
        return (int)((v / width) * 600.0);
    }

    public static String rgbToHex(int color){
        return String.format("#%06X", 0xFFFFFF & color);
    }
    public static int hexToRgb(String hex){
        return Color.parseColor(hex);
    }
}
