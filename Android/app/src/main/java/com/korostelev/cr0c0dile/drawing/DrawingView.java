package com.korostelev.cr0c0dile.drawing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.korostelev.cr0c0dile.DrawingActivity;
import com.korostelev.cr0c0dile.model.GameMessage;
import com.korostelev.cr0c0dile.model.Line;

import java.util.ArrayList;

public class DrawingView extends View {



    private int paintColor = Color.BLACK;

    public static int MIN_STROKE_WIDTH = 20;

    private int strokeWidth = MIN_STROKE_WIDTH;

    private Paint drawPaint;

    private Path path;


    //temporal
    private float startDrawPointX, startDrawPointY;

    private DrawingActivity drawingActivity;

    private boolean active = true;

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive(){
        return this.active;
    }

    private ArrayList<Path> pathPenList = new ArrayList<>();
    private ArrayList<Paint> paintPenList = new ArrayList<>();

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        createPath();
    }

    private void setupPaint() {
        // Setup paint with color and stroke styles
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(strokeWidth);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (int i = 0; i < paintPenList.size(); i++) {
            canvas.drawPath(pathPenList.get(i), paintPenList.get(i));
        }
    }

    public void setDrawingActivity(DrawingActivity drawingActivity) {
        this.drawingActivity = drawingActivity;
    }

    public void drawLine(GameMessage gameMessage){
        Line line = gameMessage.getLine();
        String color = gameMessage.getColor();
        String lineWidth = gameMessage.getLineSize();

        if(gameMessage.getClearCanvas() != null){
            resetCanvas();
        }

        if(lineWidth != null){
            int width = Integer.parseInt(lineWidth);
            if(width < MIN_STROKE_WIDTH){
                width = MIN_STROKE_WIDTH;
            }
            this.strokeWidth = width;
            drawPaint.setStrokeWidth(strokeWidth);
        }
        if(color != null) {
            paintColor = Color.parseColor(color);
            drawPaint.setColor(paintColor);
        }
        setupPaint();

        if(line != null) {
            int x = line.getX() + (getWidth() - 600) / 2;
            int y = line.getY() + (getHeight() - 600) / 2;
            int x_end = x + line.getDx();
            int y_end = y + line.getDy();
            path.moveTo(x, y);
            path.lineTo(x_end, y_end);
            createPath();
        }

        invalidate();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(!active) return true;

        float pointX = event.getX();
        float pointY = event.getY();
        // Checks for the event that occurs
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startDrawPointX = pointX;
                startDrawPointY = pointY;

                path.moveTo(pointX, pointY);
                return true;
            case MotionEvent.ACTION_MOVE:
                int x_start = DrawingUtils.normCoordinate(startDrawPointX, getWidth());
                int y_start = DrawingUtils.normCoordinate(startDrawPointY, getHeight());

                int dx = x_start - DrawingUtils.normCoordinate(pointX, getWidth());
                int dy = y_start - DrawingUtils.normCoordinate(pointY, getHeight());

                GameMessage msg = new GameMessage(new Line(x_start, y_start, dx, dy), null);
                drawingActivity.sendGameMessage(msg);

                path.lineTo(pointX, pointY);
                startDrawPointX = pointX;
                startDrawPointY = pointY;
                break;
            case  MotionEvent.ACTION_UP:
                createPath();
                break;
            default:
                return false;
        }
        postInvalidate();
        return true;
    }


    private void createPath(){
        path = new Path();
        setupPaint();
        pathPenList.add(path);
        paintPenList.add(drawPaint);
    }

    public void resetCanvas(){
        pathPenList.clear();
        paintPenList.clear();
        createPath();
        startDrawPointX = 0;
        startDrawPointY = 0;
        invalidate();
    }

    public void clearCanvas(){
        GameMessage msg = new GameMessage();
        msg.setClearCanvas("t");
        drawingActivity.sendGameMessage(msg);
        resetCanvas();
    }

    public void setLineWidth(int width){
        if(width < MIN_STROKE_WIDTH){
            width = MIN_STROKE_WIDTH;
        }
        strokeWidth = width;
        drawPaint.setStrokeWidth(strokeWidth);
        GameMessage msg = new GameMessage();
        msg.setLineSize(String.valueOf(width));
        drawingActivity.sendGameMessage(msg);
    }

    public void undoDrawing(){
        if(pathPenList.size() > 1){
            pathPenList.remove(pathPenList.size() - 2);
            paintPenList.remove(paintPenList.size() - 2);
        }
        invalidate();
    }

    public void setDrawColor(int color) {
        paintColor = color;
        drawPaint.setColor(color);
        GameMessage msg = new GameMessage();
        msg.setColor(DrawingUtils.rgbToHex(color));
        drawingActivity.sendGameMessage(msg);
    }
}
