package com.korostelev.cr0c0dile.model;
import androidx.annotation.NonNull;

public enum ConnectionStatus {
    MASTER("MASTER"),
    GUESSER("GUESSER"),
    FAIL("FAIL");

    private final String name;

    ConnectionStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
