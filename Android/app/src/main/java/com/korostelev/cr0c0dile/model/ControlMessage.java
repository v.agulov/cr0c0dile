package com.korostelev.cr0c0dile.model;

public class ControlMessage {
    private String word;
    private ConnectionStatus role;

    public ControlMessage(String word, ConnectionStatus role) {
        this.word = word;
        this.role = role;
    }

    public String getWord() {
        return word;
    }

    public ConnectionStatus getRole() {
        return role;
    }
}
