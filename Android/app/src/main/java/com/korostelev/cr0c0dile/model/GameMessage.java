package com.korostelev.cr0c0dile.model;

public class GameMessage {
    private Line line;
    private String word;
    private String clearCanvas;
    private String guesserName;
    private String color;
    private String lineSize;

    public GameMessage() {
    }


    public GameMessage(String word) {
        this.line = null;
        this.word = word;
        this.guesserName = null;
        this.color = null;
        this.lineSize = null;
    }

    public String getGuesserName() {
        return guesserName;
    }

    public GameMessage(Line line, String clearCanvas) {
        this.line = line;
        this.clearCanvas = clearCanvas;
        this.word = null;
        this.guesserName = null;
        this.color = null;
        this.lineSize = null;
    }

    public void setLineSize(String lineSize) {
        this.lineSize = lineSize;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setClearCanvas(String clearCanvas) {
        this.clearCanvas = clearCanvas;
    }

    public String getColor() {
        return color;
    }

    public String getLineSize() {
        return lineSize;
    }

    public Line getLine() {
        return line;
    }

    public String getWord() {
        return word;
    }

    public String getClearCanvas() {
        return clearCanvas;
    }
}
