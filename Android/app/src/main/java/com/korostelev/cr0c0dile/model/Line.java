package com.korostelev.cr0c0dile.model;

public class Line {
    private int x;
    private int y;
    private int dx;
    private int dy;
    public Line(int x, int y, int dx, int dy) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }
}
