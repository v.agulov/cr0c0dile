package com.korostelev.cr0c0dile.model;

public class User {
    private String name;
    private String room;
    private ConnectionStatus status;
    private String word;

    public User(String name, String room, ConnectionStatus status, String word) {
        this.name = name;
        this.room = room;
        this.status = status;
        this.word = word;
    }

    public String getName() {
        return name;
    }

    public String getRoom() {
        return room;
    }

    public ConnectionStatus getStatus() {
        return status;
    }

    public String getWord() {
        return word;
    }
}
