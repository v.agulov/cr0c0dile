package com.korostelev.cr0c0dile.network;

import android.app.Activity;
import android.app.Application;

import com.korostelev.cr0c0dile.model.User;

public class GameSession extends Application {

    private  StompConnection connection;
    private  User user;
    private Activity drawingActivity;


    public  StompConnection getStompConnection(){
        if(null == connection){
            connection = new StompConnection();
        }
        return connection;
    }

    public void restart(){
        connection = new StompConnection();
    }

    public void updateUser(User sessionUser){
        user = sessionUser;
    }

    public void updateDrawingActivity(Activity activity){drawingActivity = activity;}

    public Activity getDrawingActivity(){return drawingActivity;}

    public User getUser(){
        return user;
    }
}
