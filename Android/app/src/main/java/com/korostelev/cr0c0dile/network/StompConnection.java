package com.korostelev.cr0c0dile.network;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.korostelev.cr0c0dile.DrawingActivity;
import com.korostelev.cr0c0dile.LoginActivity;
import com.korostelev.cr0c0dile.R;
import com.korostelev.cr0c0dile.model.ConnectionStatus;
import com.korostelev.cr0c0dile.model.ControlMessage;
import com.korostelev.cr0c0dile.model.GameMessage;
import com.korostelev.cr0c0dile.model.Session;
import com.korostelev.cr0c0dile.model.User;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.CompletableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.dto.StompHeader;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompMessage;


public class StompConnection {



    private static String RECEIVE = "/topic/session";
    private static String DRAW = "/draw";
    private static String SEND = "/app/game";
    private static String CHAT = "/chat";
    private static String MASTER = "/master";
    private static final String LOGIN = "login";




    private static final String TAG = "Stomp connection";
    private static final String STOMP_ERROR = "Stomp error connection";

    private StompClient stompClient;

    private  GameSession gameSession;
    private User user;


    private Gson gson = new GsonBuilder().create();
    private CompositeDisposable compositeDisposable;

    public StompClient getStompClient(){
        return stompClient;
    }

    public void connect(String userName, String roomName, Activity activity) {

        stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://" + "192.168.0.75"
                + ":" + 8081 + "/game/websocket");

        gameSession = (GameSession) activity.getApplicationContext();


        resetSubscriptions();

        resetSubscriptions();

        Disposable lifecycle = stompClient.lifecycle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(lifecycleEvent -> {
                    switch (lifecycleEvent.getType()) {
                        case OPENED:
                            Log.d(TAG, "Stomp connection opened");
                            break;
                        case ERROR:
                            Log.e(TAG, STOMP_ERROR, lifecycleEvent.getException());
                            Toast.makeText(activity, activity.getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                            activity.findViewById(R.id.loading_panel).setVisibility(View.GONE);
                            break;
                        case CLOSED:
                            resetSubscriptions();
                            Toast.makeText(activity, activity.getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                            activity.findViewById(R.id.loading_panel).setVisibility(View.GONE);
                            break;
                        case FAILED_SERVER_HEARTBEAT:
                            Toast.makeText(activity, activity.getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                            activity.findViewById(R.id.loading_panel).setVisibility(View.GONE);
                            break;
                    }
                },  throwable -> Log.e(TAG, "Throwable " + throwable.getMessage()));

        compositeDisposable.add(lifecycle);
        stompClient.connect();


        handleRoleReply(userName, roomName, activity);
        sendConnectionInfo(userName, roomName);
    }



    private void sendConnectionInfo(String userName, String roomName) {

        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(StompHeader.DESTINATION, "/app/connect"));
        headers.add(new StompHeader(LOGIN, userName));
        StompMessage msg = new StompMessage("SEND", headers, gson.toJson(new Session(roomName)));

        compositeDisposable.add(stompClient.send(msg)
                .compose(applySchedulers())
                .subscribe(() -> Log.d(TAG, "Send connection info success"), throwable -> {
                    Log.e(TAG, STOMP_ERROR, throwable);
                }));
    }


    private void handleRoleReply(String userName, String roomName, Activity activity){
        Disposable replyTopic = stompClient.topic("/user/queue/reply")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {

                    ControlMessage controlMessage =  gson.fromJson(topicMessage.getPayload(), ControlMessage.class);

                    if(user == null) {
                        String word = controlMessage.getWord();
                        ConnectionStatus status = controlMessage.getRole();

                        Log.d(TAG, "Word to guess is: " + word);
                        Log.d(TAG, "Role is: " + status.getName());


                        user = new User(userName, roomName, status, word);
                        gameSession.updateUser(user);

                        if (status == ConnectionStatus.GUESSER) {
                            subscribeDrawEvent();
                        } else {
                            subscribeMasterEvents();
                        }
                        subscribeChat();
                        activity.findViewById(R.id.loading_panel).setVisibility(View.GONE);
                        ((LoginActivity) activity).startDrawingActivity();
                    }else{
                        handleRoleChange(controlMessage);
                    }

                }, throwable -> {
                    Toast.makeText(activity, activity.getString(R.string.connection_failed), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, STOMP_ERROR, throwable);
                });
        compositeDisposable.add(replyTopic);
    }

    private void handleRoleChange(ControlMessage controlMessage){
        String word = controlMessage.getWord();
        ConnectionStatus status = controlMessage.getRole();

        user = new User(user.getName(), user.getRoom(), status, word);
        gameSession.updateUser(user);

        if(status == ConnectionStatus.GUESSER){
            subscribeDrawEvent();
        }

        DrawingActivity drawingActivity = (DrawingActivity) gameSession.getDrawingActivity();
        drawingActivity.setGameRole();

    }





    private void subscribeMasterEvents(){
        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(LOGIN, user.getName()));
        Disposable drawTopic = stompClient.topic(RECEIVE + '/' + user.getRoom() + MASTER, headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {
                    ControlMessage controlMessage =  gson.fromJson(topicMessage.getPayload(), ControlMessage.class);
                    handleRoleChange(controlMessage);
                }, throwable -> Log.e(TAG, STOMP_ERROR, throwable));
        compositeDisposable.add(drawTopic);
    }



    public void sendChatMessage(GameMessage gameMessage){
        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(StompHeader.DESTINATION, SEND + '/' + user.getRoom() + CHAT));
        headers.add(new StompHeader(LOGIN, user.getName()));
        StompMessage msg = new StompMessage("SEND", headers, gson.toJson(gameMessage));

        Disposable msgDisposable = stompClient.send(msg)
                .doOnError(throwable -> {
                })
                .subscribe(() -> Log.d(TAG, "Send chat message success."), throwable -> Log.e(TAG, STOMP_ERROR, throwable));
        compositeDisposable.add(msgDisposable);
    }

    public void sendGameMessageOnDraw(GameMessage gameMessage){

        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(StompHeader.DESTINATION, SEND + '/' + user.getRoom() + DRAW));
        headers.add(new StompHeader(LOGIN, user.getName()));

        StompMessage msg = new StompMessage("SEND", headers, gson.toJson(gameMessage));

        Disposable msgDisposable = stompClient.send(msg)
                .doOnError(throwable -> {
                })
                .subscribe(() -> {
                    Log.d(TAG, "Send game message success");
                }, throwable -> {
                    Log.e(TAG, STOMP_ERROR, throwable);
                });
        compositeDisposable.add(msgDisposable);
    }

    private void subscribeDrawEvent(){
        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(LOGIN, user.getName()));

        Disposable drawTopic = stompClient.topic(RECEIVE + '/' + user.getRoom() + DRAW, headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {

                    DrawingActivity drawingActivity = (DrawingActivity) gameSession.getDrawingActivity();
                    if(drawingActivity == null){
                        return;
                    }

                    GameMessage msg =  gson.fromJson(topicMessage.getPayload(), GameMessage.class);
                    drawingActivity.updateCanvas(msg);


                }, throwable -> {
                    Log.e(TAG, STOMP_ERROR, throwable);
                });
        compositeDisposable.add(drawTopic);
    }

    private void subscribeChat(){
        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader(LOGIN, user.getName()));

        Disposable drawTopic = stompClient.topic(RECEIVE + '/' + user.getRoom() + CHAT, headers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {

                    DrawingActivity drawingActivity = (DrawingActivity) gameSession.getDrawingActivity();
                    if(drawingActivity == null){
                        return;
                    }

                    GameMessage msg =  gson.fromJson(topicMessage.getPayload(), GameMessage.class);
                    drawingActivity.updateChat(msg);


                }, throwable -> {
                    Log.e(TAG, STOMP_ERROR, throwable);
                });
        compositeDisposable.add(drawTopic);
    }



    private CompletableTransformer applySchedulers() {
        return upstream -> upstream
                .unsubscribeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        compositeDisposable = new CompositeDisposable();
    }
}
