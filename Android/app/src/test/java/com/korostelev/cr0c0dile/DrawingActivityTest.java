package com.korostelev.cr0c0dile;


import android.os.Build;
import android.view.View;

import androidx.test.core.app.ApplicationProvider;
import com.korostelev.cr0c0dile.model.ConnectionStatus;
import com.korostelev.cr0c0dile.model.User;
import com.korostelev.cr0c0dile.network.GameSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;


@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O_MR1)
public class DrawingActivityTest {

    private DrawingActivity activity;

    @Before
    public void setUp(){
        GameSession gameSession = ApplicationProvider.getApplicationContext();
        gameSession.restart();
        LoginActivity loginActivity = Robolectric.buildActivity(LoginActivity.class).create().resume().get();
        String room = String.valueOf(Math.random());
        gameSession.getStompConnection().connect("user2", room, loginActivity);
        gameSession.updateUser(new User("user2", room, ConnectionStatus.MASTER, "word"));
        activity = Robolectric.buildActivity(DrawingActivity.class).create().resume().get();
    }

    @Test
    public void checkCreate(){
        assertNotNull(activity);
    }

    @Test
    public void checkMasterRoleUiActive(){
        assertEquals(activity.findViewById(R.id.btn_send).getVisibility(), View.VISIBLE);
        assertEquals(activity.findViewById(R.id.reset_btn).getVisibility(), View.VISIBLE);
    }

    @Test
    public void checkBrushDialog(){
        activity.findViewById(R.id.brush_button).callOnClick();
    }
}
