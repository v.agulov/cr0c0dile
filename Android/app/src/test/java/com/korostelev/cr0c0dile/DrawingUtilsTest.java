package com.korostelev.cr0c0dile;


import android.graphics.Color;
import android.os.Build;

import com.korostelev.cr0c0dile.drawing.DrawingUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O_MR1)
public class DrawingUtilsTest {
    @Test
    public void colorFormatTest() {
        assertEquals(Color.CYAN, DrawingUtils.hexToRgb(DrawingUtils.rgbToHex(Color.CYAN)));
        assertEquals(Color.GRAY, DrawingUtils.hexToRgb(DrawingUtils.rgbToHex(Color.GRAY)));
    }
}
