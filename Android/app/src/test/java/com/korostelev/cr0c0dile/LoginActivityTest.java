package com.korostelev.cr0c0dile;


import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;

import com.korostelev.cr0c0dile.network.GameSession;
import com.rengwuxian.materialedittext.MaterialEditText;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O_MR1)
public class LoginActivityTest {


    private LoginActivity activity;
    private MaterialEditText userField;
    private MaterialEditText roomField;
    private Button loginButton;


    private void setUpActivity(){
        activity = Robolectric.buildActivity(LoginActivity.class).create().resume().get();
        userField =  activity.findViewById(R.id.user_name);
        roomField = activity.findViewById(R.id.room_name);
        loginButton = activity.findViewById(R.id.login);
    }

    @Test
    public void checkVisibleViews() {
        setUpActivity();
        assertEquals(userField.getVisibility(), View.VISIBLE);
        assertEquals(roomField.getVisibility(), View.VISIBLE);
        assertEquals(loginButton.getVisibility(), View.VISIBLE);
    }

    @Test
    public void validInputShouldCreateConnection() {
        setUpActivity();
        userField.setText("user");
        roomField.setText("1");
        loginButton.callOnClick();
        assertEquals(activity.findViewById(R.id.loading_panel).getVisibility(), View.VISIBLE);
    }

    @Test
    public void shouldWrongInputFields() {
        setUpActivity();
        userField.setText("");
        roomField.setText("");
        loginButton.callOnClick();
        assertTrue(activity.findViewById(R.id.loading_panel).getVisibility() != View.VISIBLE);
    }

    @Test
    public void shouldEmptyInputFields() {
        setUpActivity();
        userField.setText(null);
        roomField.setText(null);
        loginButton.callOnClick();
        assertTrue(activity.findViewById(R.id.loading_panel).getVisibility() != View.VISIBLE);
    }
}
