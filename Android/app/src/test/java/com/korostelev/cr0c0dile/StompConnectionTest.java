package com.korostelev.cr0c0dile;


import android.os.Build;
import android.view.View;

import com.korostelev.cr0c0dile.model.GameMessage;
import com.korostelev.cr0c0dile.network.StompConnection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import static junit.framework.TestCase.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O_MR1)
public class StompConnectionTest {

    private LoginActivity activity;
    StompConnection connection;

    @Before
    public void setUp(){
        activity = Robolectric.buildActivity(LoginActivity.class).create().resume().get();
        connection = new StompConnection();
    }

    @Test
    public void checkStompConnect(){
        connection.connect("user", "1", activity);
        assertNotNull(connection.getStompClient());
    }

    @Test
    public void checkMessageSend(){
        setUp();
        connection.connect("user", "3", activity);
        assertNotNull(connection.getStompClient());
        StompConnection clientConnection = new StompConnection();
        clientConnection.connect("user_client", "3", activity);
    }
}
