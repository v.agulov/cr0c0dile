package ru.nsu.fit.crcdil.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import ru.nsu.fit.crcdil.domain.database.TypeOfTable;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DBInitializer {
    @Value("${database.nouns_source}")
    private String nounPath;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private static final Logger logger = LoggerFactory.getLogger(DBInitializer.class.getName());

    @PostConstruct
    public void initBD() {
        try {
            TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
            TimeZone.setDefault(timeZone);
            Locale.setDefault(Locale.ENGLISH);
            MapSqlParameterSource source = new MapSqlParameterSource();

            jdbcTemplate.update("CREATE TABLE IF NOT EXISTS WORDS ( " +
                    "id SERIAL, " +
                    "word VARCHAR(100) UNIQUE NOT NULL," +
                    "PRIMARY KEY (id)" +
                    ")", source);

            addWordsFromFile(nounPath);
        }
        catch (Exception e){
            logger.error(e.toString());
        }

    }

    public void addWordsFromFile(String[] path){
        for (String s : path) {
            addWordsFromFile(s);
        }
    }

    private void addWordsFromFile(String path){
        File file = new File(path);
        try (FileReader fr = new FileReader(file);
             BufferedReader reader = new BufferedReader(fr)
        ) {
            List<String> listForWords = new LinkedList<>();
            TypeOfTable type = TypeOfTable.WORDS;
            String[] parts;
            String line = reader.readLine();
            if (line.equalsIgnoreCase("существительные")) {
                type = TypeOfTable.NOUN;
                line = reader.readLine();
            }
            else if (line.equalsIgnoreCase("глаголы")) {
                type = TypeOfTable.VERB;
                line = reader.readLine();
            }
            else if (line.equalsIgnoreCase("прилагательные")) {
                type = TypeOfTable.ADJECTIVE;
                line = reader.readLine();
            }
            while (line != null){
                line = line.toLowerCase();
                parts = line.split(" ");
                listForWords.add(parts[0]);
                line = reader.readLine();
            }
            addWords(listForWords, type);
        }
        catch (Exception e){
            logger.error(e.toString());
        }
    }

    private void addWords(List<String> list, TypeOfTable type){
        try {
            for (String s : list) {
                String word = s.toLowerCase();
                addWords(word, type);
                if (type != TypeOfTable.WORDS) {
                    addWords(word, TypeOfTable.WORDS);
                }
            }
        }
        catch (Exception e){
            logger.error(e.toString());
        }
    }


    private void addWords(String word, TypeOfTable type){
        try{
            if(type != TypeOfTable.WORDS){
                addWords(word.toLowerCase(), TypeOfTable.WORDS);
            }
            MapSqlParameterSource source = new MapSqlParameterSource()
                    .addValue("word", word);
            jdbcTemplate.update("INSERT INTO " + type + " (word) VALUES (:word)", source);
        }
        catch (Exception e){
            logger.debug(e.toString());
        }
    }


}
