let Mycanvas = require('../canvas.js');
var jsdom = require("jsdom");
var JSDOM = jsdom.JSDOM;
//mocha.setup('bdd');
var chai = require('chai');
var assert = chai.assert;
global.document = new JSDOM().window.document;
describe("isEditable", function () {
    it("проверка сеттера Editable", function () {
        let canvas = new Mycanvas(null);
        let result = true;
        canvas.isEditable(result)
        assert.equal(canvas._isEditable, result);
    });
});

describe("setListener", function () {
    it("проверка сеттера setListener", function () {
        let canvas = new Mycanvas(null);
        let result = () => {
        };
        canvas.setListener(result)
        assert.equal(canvas._listener, result);
    });
});

describe("removeListener", function () {
    it("проверка removeListener", function () {
        let canvas = new Mycanvas(null);
        let result = () => {
        };
        canvas.setListener(result)
        assert.equal(canvas._listener, result);
        canvas.removeListener();
        assert.equal(canvas._listener, null);
    });
});

describe("setPaintSize", function () {
    it("проверка setPaintSize", function () {
        let canvas = new Mycanvas(null);
        canvas.setPaintSize(3, false);
        assert.equal(canvas._size, 3);
    });
});

describe("setColor", function () {
    it("проверка setPaintSize", function () {
        let canvas = new Mycanvas(null);
        canvas.setColor(10, 11, 12, false);
        assert.equal(canvas._r, 10);
        assert.equal(canvas._g, 11);
        assert.equal(canvas._b, 12);
    });
});

describe("clearImage", function () {
    it("проверка слушателя clearImage", function () {
        let listener = (data) => {
            assert.equal(data.clearCanvas, true);
        };
        let htmlCanvas = document.createElement('canvas');
        let canvas = new Mycanvas(htmlCanvas);
        canvas.clearImage(false);
    });
});