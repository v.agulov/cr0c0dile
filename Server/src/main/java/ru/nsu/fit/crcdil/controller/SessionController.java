package ru.nsu.fit.crcdil.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Controller;
import ru.nsu.fit.crcdil.domain.logic.GameMessage;
import ru.nsu.fit.crcdil.domain.logic.WordMessageHandlingResult;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionMessage;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus;
import ru.nsu.fit.crcdil.domain.websocket.ControlMessage;
import ru.nsu.fit.crcdil.service.SessionService;

@Controller
public class SessionController implements ChannelInterceptor {
    private final SessionService sessionService;
    @Lazy
    @Autowired
    private SimpMessagingTemplate template;

    @Value("${session_path}")
    private String sessionPath;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    // На этом этапе происходит
    @MessageMapping("/connect")
    @SendToUser("/queue/reply")
    public ControlMessage connect(Message<ConnectionMessage> message) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        accessor.getReplyChannel();
        return sessionService.connect(message);
    }

    @MessageMapping("/game/{sessionName}/draw")
    @SendToUser("/queue/reply")
    public ControlMessage imageTransferController(@DestinationVariable String sessionName,
                                               @Header("login") String login,
                                               @Header("simpSessionId") String sessionId,
                                               GameMessage gameMessage) {
        gameMessage = sessionService.handleImageMessage(sessionName, login, sessionId, gameMessage);
        if(null != gameMessage) {
            template.convertAndSend(sessionPath + sessionName + "/draw", gameMessage);
            return null;
        }
        return new ControlMessage(null, ConnectionStatus.GUESSER);
    }

    @MessageMapping("/game/{sessionName}/chat")
    @SendToUser("/queue/reply")
    public ControlMessage wordTransferController(@DestinationVariable String sessionName,
                                                 @Header("login") String login,
                                                 GameMessage message) {
        WordMessageHandlingResult result = sessionService.handleWordMessage(sessionName, login, message);
        if(null != result.getGameMessage()) {
            template.convertAndSend(sessionPath + sessionName + "/chat", result.getGameMessage());
        }
        if(null != result.getMasterLogin()) {
            template.convertAndSend(sessionPath + sessionName + "/master/" + result.getMasterLogin(),
                    result.getMessageToMaster());
        }
        return result.getMessageToGuesser();
    }


    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        if(sessionService.validatePreSend(message)) {
            return message;
        }
        return null;
    }
}
