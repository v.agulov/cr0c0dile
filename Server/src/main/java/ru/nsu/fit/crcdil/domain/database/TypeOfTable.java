package ru.nsu.fit.crcdil.domain.database;

public enum TypeOfTable {
    WORDS,
    NOUN,
    VERB,
    ADJECTIVE
}

