package ru.nsu.fit.crcdil.domain.logic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameMessage {
    private Line line;
    private String word;
    private String clearCanvas;
    private String guesserName;
    private String color;
    private String lineSize;

    public GameMessage() {
    }

    public GameMessage(Line line, String word) {
        this.line = line;
        this.word = word;
        this.guesserName = null;
        this.clearCanvas = null;
        this.color = null;
        this.lineSize = null;
    }
}
