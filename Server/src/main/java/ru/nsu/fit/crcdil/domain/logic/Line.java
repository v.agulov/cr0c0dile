package ru.nsu.fit.crcdil.domain.logic;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Line {
    private int x;
    private int y;
    private int dx;
    private int dy;
}
