package ru.nsu.fit.crcdil.domain.logic;

import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.nsu.fit.crcdil.domain.exceptions.UserNotFoundException;
import ru.nsu.fit.crcdil.service.WordsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Session {
    private WordsService wordsService;
    private String name;
    private boolean isStarted;
    private Map<String, User> users;
    private List<Line> imageHistory;
    private User master;

    public Session(String name, NamedParameterJdbcTemplate jdbcTemplate) {
        master = null;
        this.name = name;
        this.wordsService = new WordsService(jdbcTemplate);
        this.users = new HashMap<>();
        this.imageHistory = new ArrayList<>();
        isStarted = false;
    }

    public boolean addUser(String name, String sessionId) {
        User user = new User(name, sessionId);
        if(users.isEmpty()) {
            master = user;
        }
        if(null != users.get(user.getName())) {
            return false;
        }
        users.put(name, user);
        return true;
    }

    public void setMaster(String name) throws UserNotFoundException {
        User user = users.get(name);
        if(null == user) {
            throw new UserNotFoundException("Session doesn't contain the user");
        }
        master = user;
    }
}
