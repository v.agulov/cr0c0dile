package ru.nsu.fit.crcdil.domain.logic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private String name;
    private String sessionId;

    public User(String name, String sessionId) {
        this.name = name;
        this.sessionId = sessionId;
    }
}
