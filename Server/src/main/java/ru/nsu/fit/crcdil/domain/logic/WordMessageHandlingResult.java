package ru.nsu.fit.crcdil.domain.logic;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.crcdil.domain.websocket.ControlMessage;

@Setter
@Getter
public class WordMessageHandlingResult {
    private GameMessage gameMessage;
    private ControlMessage messageToGuesser;
    private ControlMessage messageToMaster;
    private String masterLogin;

    public WordMessageHandlingResult() {
        gameMessage = null;
        messageToGuesser = null;
        messageToMaster = null;
        masterLogin = null;
    }
}
