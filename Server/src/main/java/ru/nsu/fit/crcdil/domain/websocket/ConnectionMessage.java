package ru.nsu.fit.crcdil.domain.websocket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConnectionMessage {
    private String sessionName;

    public ConnectionMessage() {
    }

    public ConnectionMessage(String sessionName) {
        this.sessionName = sessionName;
    }
}
