package ru.nsu.fit.crcdil.domain.websocket;

import java.util.Arrays;

public enum ConnectionStatus {
    MASTER("MASTER"),
    GUESSER("GUESSER"),
    FAIL("FAIL");

    private final String name;

    ConnectionStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ConnectionStatus valueOfName(String name) {
        return Arrays.stream(ConnectionStatus.values()).filter(connectionStatus -> connectionStatus.name.equals(name)).findAny().orElse(null);
    }
}
