package ru.nsu.fit.crcdil.domain.websocket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ControlMessage {
    private String word;
    private ConnectionStatus role;

    public ControlMessage(String word, ConnectionStatus role) {
        this.word = word;
        this.role = role;
    }
}
