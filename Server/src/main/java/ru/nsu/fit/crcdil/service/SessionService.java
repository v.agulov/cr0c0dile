package ru.nsu.fit.crcdil.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;
import ru.nsu.fit.crcdil.domain.exceptions.UserNotFoundException;
import ru.nsu.fit.crcdil.domain.logic.GameMessage;
import ru.nsu.fit.crcdil.domain.logic.Session;
import ru.nsu.fit.crcdil.domain.logic.User;
import ru.nsu.fit.crcdil.domain.logic.WordMessageHandlingResult;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionMessage;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus;
import ru.nsu.fit.crcdil.domain.websocket.ControlMessage;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SessionService {

    private Map<String, Session> sessionMap = new HashMap<>();
    private final Object sessionMapMutex = new Object();
    private static final Logger logger = LoggerFactory.getLogger(Session.class.getName());
    private final NamedParameterJdbcTemplate jdbcTemplate;


    public ControlMessage connect(Message<ConnectionMessage> message) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
        String sessionName = HtmlUtils.htmlEscape(message.getPayload().getSessionName());
        String userName = headerAccessor.getLogin();
        ConnectionStatus role = ConnectionStatus.GUESSER;
        String initialWord = null;
        synchronized (sessionMapMutex) {
            Session session = sessionMap.get(sessionName);
            if (null != session) {
                if(!session.isStarted()) {
                    if(!session.addUser(userName, headerAccessor.getSessionId())) {
                        role = ConnectionStatus.FAIL;
                    } else {
                        // пока что искусственное ограничение
                        if (session.getUsers().size() > 2) {
                            session.setStarted(true);
                        }
                    }
                }
                else {
                    role = ConnectionStatus.FAIL;
                }
            } else {
                session = new Session(sessionName, jdbcTemplate);
                session.addUser(userName, headerAccessor.getSessionId());
                initialWord = session.getWordsService().getOtherWord();
                sessionMap.put(sessionName, session);
                role = ConnectionStatus.MASTER;
            }
        }
        String extraInfo = (null == initialWord ? "" : "; word to guess is ") + initialWord;
        logger.info("{} connect. Session {}; User {} ({}){}",
                role, sessionName, userName, headerAccessor.getSessionId(), extraInfo);
        return new ControlMessage(initialWord, role);
    }

    public boolean validatePreSend(Message<?> message) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
        if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
            return null != headerAccessor.getDestination() && (headerAccessor.getDestination().equals("/user/queue/reply") ||
                    validateSubscription(headerAccessor.getLogin(), headerAccessor.getDestination(), headerAccessor.getSessionId()));
        }
        else return !StompCommand.SEND.equals(headerAccessor.getCommand()) ||
                null != headerAccessor.getDestination() && (headerAccessor.getDestination().equals("/app/connect") ||
                validateSend(headerAccessor.getLogin(), headerAccessor.getDestination(), headerAccessor.getSessionId()));
    }

    public WordMessageHandlingResult handleWordMessage(String sessionName, String user, GameMessage gameMessage) {
        WordMessageHandlingResult result = new WordMessageHandlingResult();
        if(null == gameMessage.getWord()) {
            return result;
        }
        Session session = sessionMap.get(sessionName);
        if(null != session) {
            boolean guessed = session.getWordsService().isWordCorrect(gameMessage.getWord());
            if(guessed) {
                try {
                    User master = session.getMaster();
                    session.setMaster(user);
                    String newWord = session.getWordsService().getOtherWord();
                    result.setMasterLogin(master.getName());
                    result.setMessageToMaster(new ControlMessage(null, ConnectionStatus.GUESSER));
                    result.setMessageToGuesser(new ControlMessage(newWord, ConnectionStatus.MASTER));
                    logger.info(" Session {}; word was guessed by {}; new word is <{}> )", sessionName, user, newWord);
                } catch (UserNotFoundException e) {
                    logger.error(e.toString());
                    return result;
                }
            } else {
                logger.info(" Session {}; guessed word is <{}> )", sessionName, gameMessage.getWord());
            }
        } else {
            return result;
        }
        gameMessage.setGuesserName(user);
        result.setGameMessage(gameMessage);
        return result;
    }

    public GameMessage handleImageMessage(String sessionName, String user, String userSession, GameMessage gameMessage) {
        Session session = sessionMap.get(sessionName);
        if(null == session) {
            return null;
        }
        User master = session.getMaster();
        if(!master.getName().equals(user) || !master.getSessionId().equals(userSession)) {
            return null;
        }
        return gameMessage;
    }











    // решает, можно ли пользователю присоединиться к сессии
    private boolean validateSubscription(String userName, String gameSession, String userSession) {
        return sessionContainsUser(userName, gameSession.replaceFirst("/topic/session/", ""), userSession);
    }

    private boolean validateSend(String userName, String gameSession, String userSession) {
        return sessionContainsUser(userName, gameSession.replaceFirst("/app/game/", ""), userSession);
    }

    // проверяет, принадлежит ли пользователь сессии
    private boolean sessionContainsUser(String userName, String gameSession, String userSession) {
        User user;
        int sessionNameLength = gameSession.indexOf('/');
        if(sessionNameLength > 0) {
            gameSession = gameSession.substring(0, sessionNameLength);
        }
        Session session = sessionMap.get(gameSession);
        if (null == session) {
            return false;
        }
        user = session.getUsers().get(userName);
        return null != user && user.getSessionId().equals(userSession);
    }
}
