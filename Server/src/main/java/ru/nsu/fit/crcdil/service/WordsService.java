package ru.nsu.fit.crcdil.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class WordsService {
    // Объект имеет состояние - текущее угадываемое слово
    private String currentWord;
    //режим игры с нечетким поиском
    private boolean fuzzyIsUsed;
    private Integer wordsCount;
    private Random random = new Random();

    @Autowired
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private static final Logger log = LoggerFactory.getLogger(WordsService.class.getName());

    public WordsService(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        wordsCount = jdbcTemplate.queryForObject("select count(*) from words", new MapSqlParameterSource(), Integer.class);
        if(wordsCount == null) {
            log.error("Words count is null");
            wordsCount = 0;
        }
    }


    // По просьбе извне необходимо перевыбрать слово и вернуть его
    public String getOtherWord(){
        try {
            String sql = "WITH wr AS ( select word, row_number() over () as rn from words)\n" +
                    "select word from wr where rn = :random";
            List<String> result = jdbcTemplate.queryForList(sql, new MapSqlParameterSource().addValue("random", random.nextInt(wordsCount)), String.class);
            return parseDatabaseOutput(result);
        }
        catch (Exception e){
            log.error(e.toString());
            return null;
        }
    }

    // Проверка слова, решает угадал ли игрок слово или нет
    public boolean isWordCorrect(String word) {
        word = word.toLowerCase();
        if(word.equalsIgnoreCase(currentWord)){
            return true;
        }
        else {
            return null != currentWord && (fuzzyIsUsed && (checkFuzzy(word)));
        }
    }

    public String getCurrentWord() {
        return currentWord;
    }

    private double levenshteinRatio(String s, String t) {
        int cost;
        int rows = s.length() + 1;
        int cols = t.length() + 1;
        int[][] distance = new int[rows][cols];
        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                distance[i][j] = 0;
            }
        }
        for(int i = 1; i < rows; i++){
            distance[i][0] = i;
        }
        for(int j = 1; j < cols; j++){
            distance[0][j] = j;
        }
        for(int i = 1; i < cols; i++) {
            for (int j = 1; j < rows; j++) {
                if(s.charAt(j - 1) == t.charAt(i - 1)){
                    cost = 0;
                }
                else{
                    cost = 1;
                }
                distance[j][i] = Math.min( distance[j-1][i-1] + cost,Math.min(distance[j-1][i] + 1, distance[j][i-1] + 1));
            }
        }
        return (double)((s.length()+t.length()) - distance[rows-1][cols-1]) / (s.length()+t.length());
    }
    
    private boolean checkFuzzy(String word){
        if(levenshteinRatio(word.toLowerCase(), currentWord.toLowerCase()) >= 0.8){
            try {
                String sql = "SELECT word FROM WORDS WHERE word = :word";
                List<String> result = jdbcTemplate.queryForList(sql, new MapSqlParameterSource().addValue("word", word), String.class);
                if (result.isEmpty()) {
                    return true;
                }
            } catch (Exception e) {
                log.error(e.toString());
                return false;
            }
        }
        return false;
    }

    private String parseDatabaseOutput(List<String> result) {
        if (null != result && !result.isEmpty()){
            currentWord = result.get(0).toLowerCase();
            currentWord = currentWord.substring(0, 1).toUpperCase() + currentWord.substring(1);
            return currentWord;
        } else {
            log.warn("Database was not return any word");
            return null;
        }
    }
    
    public void setFuzzyIsUsed(boolean fuzzyIsUsed){
        this.fuzzyIsUsed = fuzzyIsUsed;
    }
}
