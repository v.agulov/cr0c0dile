var stompClient = null;
let sessionName;
let userName;
let send = "/app/game";
let recv = "/topic/session";
let draw = "/draw";
let chat = "/chat";
let master = "/master";
let NOT_CONNECTED = "FAIL";
let MASTER = "MASTER";
let GUESSER = "GUESSER";
let canvas;
let role = NOT_CONNECTED;
let drawConnection = null;
let chatConnection = null;
let masterConnection = null;
///////////////////////////
let chatArea;
let submitBtn;
let inputField;
let infoText;
let clearBtn;
let changeRole;
let listener = (data) => {
    if (stompClient !== null) {
        // Отправляю игре с указанным именем сессии
        stompClient.send(send + '/' + sessionName + draw, {login: userName}, JSON.stringify(data));
        //console.log("send: " + JSON.stringify(data));
    }
};
///////////////////////////
//inputField.readOnly=true;
//document.getElementById("chatBox").value = '';
//chatArea.html("");
//submitBtn.prop("disabled",true);
window.addEventListener("load", function onWindowLoad() {
    canvas = new Mycanvas(document.getElementById("sheet"));
    $("#LineSize").prop("disabled", true);
    $("#LineSize").val(canvas._size);
    infoText = $("#info");
    chatArea = $("#chatBox");
    inputField = $("#inputBox");
    submitBtn = $("#submit");
    clearBtn = $("#clearBtn");
    inputField.prop("disabled", true);
    infoText.html("Hello!");
    resetSessionUI();
    let o = document.createElement('div');
    o.setAttribute("style","margin:10px 10px 10px 10px");
    generatePalette(o);
    document.getElementById("canvasTools").appendChild(o);
    //document.getElementById("canvasTools").appendChild(createColorBtn(null, 255,255,255));

    //document.getElementById("chatBox").setAttribute("nodeValue","")
});

function generatePalette(palette) {
    let maxR = 3;//3;
    let maxG = 3;//5;
    let maxB = 3;//2;
    let cursor = 0;
    let row = document.createElement('div');
    row.setAttribute("class", "form-group");
    for (let r = 0; r <= maxR; r++) {
        for (let g = 0; g <= maxG; g++) {
            for (let b = 0; b <= maxB; b++) {
                let rColor = Math.round(r * 255 / maxR);
                let gColor = Math.round(g * 255 / maxG);
                let bColor = Math.round(b * 255 / maxB);
                let btn = createColorBtn(palette, rColor, gColor, bColor);
                btn.onclick = function () {
                    if(role===MASTER) {
                        canvas.setColor(rColor, gColor, bColor, true);
                    }
                };
                row.appendChild(btn);
                cursor += 1;
                if (cursor === 4) {
                    palette.appendChild(row);
                    row = document.createElement('div');
                    row.setAttribute("class", "form-group");
                    cursor = 0;
                }
            }
        }
    }
}

function createColorBtn(palette, r, g, b) {
    //<div class="colorBox" id="colorBox" style=" background: rgb(122,122,122);"></div>
    let box = document.createElement('div');
    box.className = 'colorBox';
    /*box.style.backgroundColor = (
        'rgb(' + r + ", "
        + g + ", "
        + b + ")"
    );*/
    box.setAttribute("style", 'background:rgb(' + r + ", "
        + g + ", "
        + b + ")");
    return box;
}
function masterMenu(isMaster) {
    canvas.isEditable(isMaster);
    clearBtn.prop("disabled", !isMaster);
    $("#LineSize").prop("disabled", !isMaster);
    inputField.prop("disabled", isMaster);
    submitBtn.prop("disabled", isMaster);
}
function resetSessionUI() {
    chatArea.val("");
    inputField.val("");
    canvas.resetParams();
    $("#LineSize").val(canvas._size);
    submitBtn.prop("disabled", true);
    clearBtn.prop("disabled", true);
    canvas.clearImage();
    canvas.isEditable(false);
    canvas.removeListener();
}
function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#name").prop("disabled", connected);
    $("#session").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
}
function connect() {
    const socket = new SockJS('/game');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('stompClient Connected: ' + frame);
        if (stompClient !== null) {
            // Подпишусь на рассылку, если подключение удастся.
            // Попытаюсь подключиться дальше
            changeRole = function (newRoleData) {
                const data = JSON.parse(newRoleData.body);
                const newRole = data.role;
                console.log("role:" + role);
                const word = data.word;
                if (newRole === MASTER || newRole === GUESSER) {
                    role = newRole;
                    unSubDraw();
                    unSubChat();
                    unSubMaster();
                    canvas.removeListener();
                    // Результат положительный, поэтому можно подписываться и ставить listener на отправку
                    // Вообще со стороны сервера я дополнительно проверяю, что пользователь принадлежит сессии
                } else {
                    role = NOT_CONNECTED;
                    console.log("FAIL!");
                    infoText.html("Connection failed! You can try again.");
                    setConnected(false);
                    return;
                }
                if (newRole === MASTER) {
                    subscribeMaster();
                    subscribeChat();
                    canvas.setListener(listener);
                    canvas.clearImage(true);
                    masterMenu(true);
                    infoText.html("word: " + word);
                } else if (newRole === GUESSER) {
                    canvas.clearImage();
                    unSubMaster();
                    subscribeChat();
                    subscribeDraw();
                    masterMenu(false);
                    infoText.html("Guess the word in the canvas");
                }
            }
            stompClient.subscribe("/user/queue/reply", changeRole);
            // Отправляю запрос на присоединение к сессии с текущим именем
            stompClient.send("/app/connect", {login: userName}, JSON.stringify({
                "sessionName": sessionName,
            }));
        }
    });
}

function disconnect() {
    unSubChat();
    unSubDraw();
    unSubMaster();
    resetSessionUI();
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
    infoText.html("Disconnected");
}
$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#submit").click(function () {
        console.log("role: " + role);
        if (role !== GUESSER) {
            //alert("Guesser method, but role is " + role);
            return;
        }
        /*
        if(inputField.val().match(/[A-Za-z0-9]/)!==true) {
            return;
        }
        */

        stompClient.send(send + "/" + sessionName + chat, {login: userName}, JSON.stringify({
            word: inputField.val()//FIXME:формат
        }));
        inputField.val("");
        document.getElementById("inputBox").focus();//FIXME:может стать источником ошибок, так как другой
        // способ доступа к кнопке.
    });
    $("#LineSize").change(function () {
        if(role === MASTER) {
            canvas.setPaintSize($("#LineSize").val(),true);
        } else {
            canvas.setPaintSize($("#LineSize").val(),false);
        }
    })
    $("#connect").click(function () {
        sessionName = $("#session").val();//FIXME:формат
        userName = $("#name").val();//FIXME:формат
        /*if(sessionName.match(/[A-Za-z0-9]/)!==true) {
            sessionName=undefined;
            userName=undefined;
            infoText.html("wrong format of Session name");
            return;
        }
        if(userName.match(/[A-Za-z0-9]/)!==true) {
            infoText.html("wrong format of user name");
            sessionName=undefined;
            userName=undefined;
            return;
        }*/
        infoText.html("Connecting...");
        $("#name").prop("disabled", true);
        $("#session").prop("disabled", true);
        $("#connect").prop("disabled", true);
        connect();
    });
    $("#disconnect").click(function () {
        infoText.html("Disconnecting...");
        disconnect();
    });
    $("#clearBtn").click(function () {
        console.log("clear");
        if(role!==MASTER) {
            //alert("Master method, but role is " + role);
        }
        canvas.clearImage(true);
    })
});

function unSubChat() {
    if (chatConnection === null) {
        /*alert("unsub error");
        console.log("unsub error");*/
        return;
    }
    console.log("unSubChat" + chatConnection);
    chatConnection.unsubscribe();
    chatConnection=null;

}

function unSubDraw() {
    if (drawConnection === null) {
        /*alert("unsub error");
        console.log("unsub error");*/
        return;
    }
    console.log("unSubDraw" + drawConnection);
    drawConnection.unsubscribe();
    drawConnection=null;
}

function subscribeChat() {
    if (chatConnection !== null) {
        //("reconnect chat error");
        console.log("reconnect chat error");
        return;
    }
    console.log("subscribe chat");
    chatConnection = stompClient.subscribe(recv + '/' + sessionName + chat, function (message) {
        const body = JSON.parse(message.body);
        const word = body.word;
        const name = body.guesserName;
        console.log("recv chat: " + body);
        console.log("chat name: " + name);
        console.log("chat word: " + word);
        if (chatArea.val() === "") {
            chatArea.val(name + ": " + word);
            return;
        }
        chatArea.val(chatArea.val() + '\n' + name + ": " + word);
        chatArea.scrollTop(chatArea[0].scrollHeight);
    }, {login: userName});
}
function subscribeMaster() {
    if (masterConnection !== null) {
        //alert("reconnect draw error");
        console.log("reconnect draw error");
        return;
    }
    if (role !== MASTER) {
        //alert("Master method, but role is " + role);
    }
    console.log("subscribe master");
    masterConnection = stompClient.subscribe(recv + '/' + sessionName + master + '/' + userName, changeRole, {login: userName});
}

function unSubMaster() {
    if (masterConnection === null) {
        return;
    }
    console.log("unSubMaster" + masterConnection);
    masterConnection.unsubscribe();
    masterConnection = null;
}

function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function subscribeDraw() {
    if (drawConnection !== null) {
        //alert("reconnect draw error");
        console.log("reconnect draw error");
        return;
    }
    console.log("subscribe draw");
    drawConnection = stompClient.subscribe(recv + '/' + sessionName + draw, function (message) {
            if (role === MASTER) {
                //alert("not Master method, but role is " + role);
            }
            const body = JSON.parse(message.body);
            const clearCanvas = body.clearCanvas;
            const color = body.color;
            const line = body.line;
            const lineSize = body.lineSize;
            if (clearCanvas) {
                canvas.clearImage();
            }
            if (lineSize!==null) {
                canvas.setPaintSize(lineSize);
                $("#LineSize").val(lineSize);
            }
            if (color !== null) {
                let rgb = hexToRgb(color);
                canvas.setColor(rgb.r, rgb.g, rgb.b, false);
            }
            if (line !== null) {
                canvas.drawLine(line.x, line.y, line.dx, line.dy);
            }
        }
        ,
        {
            login: userName
        }
    );
}
