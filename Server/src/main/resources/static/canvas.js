class Mycanvas {
    constructor(canvas) {
        this._canvas = canvas;
        this._x = 0;
        this._y = 0;
        this._isEditable = false;
        this._r = 0;
        this._g = 0;
        this._b = 0;
        this._size = 10;
        /*this._canvas.onmousemove*/
        document.body.onmousemove = (e) => {
            if (this._isEditable) {
                let doc =  document.documentElement;
                let rect = canvas.getBoundingClientRect();
                const x = e.pageX - rect.left - doc.scrollLeft;
                const y = e.pageY - rect.top - doc.scrollTop;
                console.log("...............");
                console.log("pageX: " + e.pageX);
                console.log("pageY: " + e.pageY);
                console.log("left: " + rect.left);
                console.log("top: " + rect.top);
                console.log("scrollLeft: " + doc.scrollLeft);
                console.log("scrollTop: " + doc.scrollTop);
                console.log("resultX: " + x);
                console.log("resultY: " + y);
                console.log("...............");
                const dx = x - this._x;//e.movementX;
                const dy = y - this._y;//e.movementY;
                if (x < 0 || x > rect.width || y < 0 || y > rect.height) {
                    if (this._x < 0 || this._x > rect.width || this._y < 0 || this._y > rect.height) {
                        this._x = x;
                        this._y = y;
                        return;
                    }
                }
                this._x = x;
                this._y = y;
                if (e.buttons > 0) {
                    this.drawLine(x, y, dx, dy);
                    if (this._listener !== undefined && this._listener !== null) {
                        this._listener({line: {x, y, dx, dy}});
                    }
                }
            }
        };
    }
    resetParams() {
        this.setPaintSize(10, false);
        this.setColor(0, 0, 0, false);
    }

    setPaintSize(size, callListener) {
        if(callListener===true) {
            if (this._listener !== undefined && this._listener !== null) {
                this._listener({"lineSize": size});
            }
        }
        this._size=size;
    }
    setColor(r, g, b, callListener) {
        this._r = r;
        this._g = g;
        this._b = b;
        console.log("click r:" + r + " g:" + g+ " b:" + b);
        if (callListener === true) {
            if (this._listener !== undefined && this._listener !== null) {
                this._listener({"color": rgbToHex(r, g, b)});
            }
        }
    }

    drawLine(x, y, dx, dy) {
        //console.log("draw x: " + x + ", y: " + y + ", dx: "+  dx + ", dy: " + dy);
        let context = this._canvas.getContext("2d");
        console.log("draw r:" + this._r + " g: " + this._g + " b: " + this._b);
        context.lineCap="round";
        context.lineWidth=this._size;
        context.strokeStyle = 'rgb(' + this._r + ',' + this._g + ',' + this._b + ')';
        context.beginPath();
        context.moveTo(x, y);
        context.lineTo(x - dx, y - dy);
        context.stroke();
        context.closePath();
    }
    clearImage(callListener) {
        if (callListener === true) {
            if (this._listener !== undefined && this._listener !== null) {
                this._listener({"clearCanvas": true});
            }
        }
        let context = this._canvas.getContext("2d");
        context.fillStyle = 'rgb(255,255,255)';
        context.clearRect(0, 0, 600, 600);
    }

    setListener(listener) {
        this._listener=listener;
    }
    removeListener() {
        this._listener=null;
    }
    isEditable(b) {
        this._isEditable=b;
    }
};