package ru.nsu.fit.crcdil.testingweb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import ru.nsu.fit.crcdil.controller.SessionController;
import ru.nsu.fit.crcdil.domain.logic.GameMessage;
import ru.nsu.fit.crcdil.domain.logic.Line;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionMessage;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus;
import ru.nsu.fit.crcdil.domain.websocket.ControlMessage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus.GUESSER;
import static ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus.MASTER;

@ContextConfiguration
@SpringBootTest
class SessionControllerTest {
    @Autowired
    private SessionController controller;

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void connectionSuccessFromService() {
        Message<ConnectionMessage> message = MessageBuilder.createMessage(new ConnectionMessage("controllerConnectSession"),
                getAccessor().getMessageHeaders());
        ControlMessage status = controller.connect(message);
        // Проверка ответа
        assertEquals(ConnectionStatus.MASTER, status.getRole());
        assertNotNull(status.getWord());
    }

    @Test
    void preSEndTest() {
        Message<ConnectionMessage> validSubscribe = MessageBuilder.createMessage(new ConnectionMessage("controllerConnectSession"),
                getAccessor(StompCommand.SUBSCRIBE, "/user/queue/reply").getMessageHeaders());
        Message<ConnectionMessage> invalidSubscribe = MessageBuilder.createMessage(new ConnectionMessage("controllerConnectSession"),
                getAccessor(StompCommand.SUBSCRIBE, "/invalid/destination").getMessageHeaders());
        assertEquals(validSubscribe, controller.preSend(validSubscribe, null));
        assertNull(controller.preSend(invalidSubscribe, null));
    }

    @Test
    void imageTransferTest() {
        GameMessage gameMessage = new GameMessage(null, "word");
        ControlMessage controlMessage = controller.imageTransferController("", "", "", gameMessage);
        assertNull(controlMessage.getWord());
        assertEquals(GUESSER, controlMessage.getRole());


        StompHeaderAccessor accessor = getAccessor();
        accessor.setLogin("master");
        accessor.setSessionId("masterSessionId");
        Message<ConnectionMessage> message = MessageBuilder.createMessage(new ConnectionMessage("imageTransferTestSession"),
                accessor.getMessageHeaders());
        ControlMessage status = controller.connect(message);
        // Проверка ответа
        assertEquals(ConnectionStatus.MASTER, status.getRole());
        assertNotNull(status.getWord());

        accessor = getAccessor();
        accessor.setLogin("guesser");
        message = MessageBuilder.createMessage(new ConnectionMessage("imageTransferTestSession"),
                accessor.getMessageHeaders());
        status = controller.connect(message);
        // Проверка ответа
        assertEquals(GUESSER, status.getRole());
        assertNull(status.getWord());
        gameMessage = new GameMessage(new Line(), null);
        controlMessage = controller.imageTransferController("imageTransferTestSession", "guesser", accessor.getSessionId(), gameMessage);
        assertNull(controlMessage.getWord());
        assertEquals(GUESSER, controlMessage.getRole());


        accessor = getAccessor();
        accessor.setLogin("master");
        accessor.setSessionId("masterSessionId");
        assertNull(controller.imageTransferController("imageTransferTestSession", accessor.getLogin(), accessor.getSessionId(), gameMessage));
    }

    @Test
    void wordTransferTest() {
        GameMessage gameMessage = new GameMessage(new Line(), null);
        assertNull(controller.wordTransferController("", "user", gameMessage));



        StompHeaderAccessor accessor = getAccessor();
        accessor.setLogin("master");
        Message<ConnectionMessage> message = MessageBuilder.createMessage(new ConnectionMessage("wordTransferTestSession"),
                accessor.getMessageHeaders());
        ControlMessage controlMessage = controller.connect(message);
        // Проверка ответа
        assertEquals(ConnectionStatus.MASTER, controlMessage.getRole());
        String choosenWord = controlMessage.getWord();
        assertNotNull(choosenWord);

        accessor = getAccessor();
        accessor.setLogin("guesser");
        accessor.setSessionId("guesserSessionId");
        message = MessageBuilder.createMessage(new ConnectionMessage("wordTransferTestSession"),
                accessor.getMessageHeaders());
        controlMessage = controller.connect(message);
        // Проверка ответа
        assertEquals(GUESSER, controlMessage.getRole());
        assertNull(controlMessage.getWord());
        gameMessage = new GameMessage(null, "__12345UnexistingWord12345__");
        controlMessage = controller.wordTransferController("wordTransferTestSession", "guesser", gameMessage);
        assertNull(controlMessage);

        gameMessage = new GameMessage(null, choosenWord);
        controlMessage = controller.wordTransferController("wordTransferTestSession", "guesser", gameMessage);
        assertEquals(MASTER, controlMessage.getRole());
        assertNotNull(controlMessage.getWord());
    }

    private StompHeaderAccessor getAccessor() {
        return getAccessor(StompCommand.SEND, null);
    }

    private StompHeaderAccessor getAccessor(StompCommand command, String destination) {
        StompHeaderAccessor accessor = StompHeaderAccessor.create(command);
        accessor.setLogin("player1");
        accessor.setSessionId("userSession1");
        accessor.setDestination(destination);
        return accessor;
    }
}
