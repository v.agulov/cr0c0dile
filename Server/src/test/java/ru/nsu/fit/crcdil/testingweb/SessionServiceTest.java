package ru.nsu.fit.crcdil.testingweb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.nsu.fit.crcdil.domain.logic.GameMessage;
import ru.nsu.fit.crcdil.domain.logic.Line;
import ru.nsu.fit.crcdil.domain.logic.WordMessageHandlingResult;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionMessage;
import ru.nsu.fit.crcdil.domain.websocket.ConnectionStatus;
import ru.nsu.fit.crcdil.domain.websocket.ControlMessage;
import ru.nsu.fit.crcdil.service.SessionService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@WebAppConfiguration
@ContextConfiguration
@SpringBootTest
public class SessionServiceTest {

    @Autowired
    private SessionService sessionService;

    @Test
    void connectionTest() {
        int count = 4;
        List<Message<ConnectionMessage>> messages = new ArrayList<>(count);
        for(int i = 0; i < count; ++i) {
            messages.add(MessageBuilder.createMessage(new ConnectionMessage("serviceConnectSession"),
                    getAccessor(StompCommand.SEND, "player" + i, "userSession" + i).getMessageHeaders()));
        }
        // При текущем искусственном закрытии эти тесты корректны.
        // Как только сессия будет стартовать нормально, их нужно будет изменить
        ControlMessage controlMessage = sessionService.connect(messages.get(0));
        assertEquals(ConnectionStatus.MASTER, controlMessage.getRole());
        assertNotNull(controlMessage.getWord());
        for(int i = 1; i < count - 1; ++i) {
            controlMessage = sessionService.connect(messages.get(i));
            assertEquals(ConnectionStatus.GUESSER, controlMessage.getRole());
            assertNull(controlMessage.getWord());
        }
        controlMessage = sessionService.connect(messages.get(count - 1));
        assertEquals(ConnectionStatus.FAIL, controlMessage.getRole());
        assertNull(controlMessage.getWord());
    }

    @Test
    void validatePreSendTest() {
        // Корректные сообщения
        Message<ConnectionMessage> validMessage = MessageBuilder.createMessage(new ConnectionMessage("servicePreSendSession"),
                getAccessor(StompCommand.SUBSCRIBE, "/user/queue/reply").getMessageHeaders());
        assertTrue(sessionService.validatePreSend(validMessage));

        validMessage = MessageBuilder.createMessage(new ConnectionMessage("servicePreSendSession"),
                getAccessor(StompCommand.SEND, "/app/connect").getMessageHeaders());
        assertTrue(sessionService.validatePreSend(validMessage));

        // Некорректные сообщения
        Message<ConnectionMessage> invalidMessage = MessageBuilder.createMessage(new ConnectionMessage("servicePreSendSession"),
                getAccessor(StompCommand.SUBSCRIBE, null).getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidMessage));
        invalidMessage = MessageBuilder.createMessage(new ConnectionMessage("servicePreSendSession"),
                getAccessor(StompCommand.SUBSCRIBE, null).getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidMessage));
    }

    @Test
    void validateSendTest() {
        // Корректные сообщения
        Message<ConnectionMessage> validSend = MessageBuilder.createMessage(new ConnectionMessage("serviceSendSession"),
                getAccessor(StompCommand.SEND, "player", "existingSession", null).getMessageHeaders());
        sessionService.connect(validSend);

        validSend = MessageBuilder.createMessage(new ConnectionMessage("serviceSendSession"),
                getAccessor(StompCommand.SEND, "player", "existingSession", "/app/game/serviceSendSession").getMessageHeaders());
        assertTrue(sessionService.validatePreSend(validSend));

        // Некорректные сообщения
        Message<ConnectionMessage> invalidSend = MessageBuilder.createMessage(new ConnectionMessage("serviceSendSession"),
                getAccessor(StompCommand.SEND, "anotherPlayer", "notExistingSession", "/app/game/serviceSendSession").getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidSend));
        invalidSend = MessageBuilder.createMessage(new ConnectionMessage("serviceSendSession"),
                getAccessor(StompCommand.SEND, "player", "notExistingSession", "/app/game/serviceSendSession").getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidSend));
    }

    @Test
    void validateSubscribeTest() {
        // Корректные сообщения
        Message<ConnectionMessage> validMessage = MessageBuilder.createMessage(new ConnectionMessage("serviceSubscribeSession"),
                    getAccessor(StompCommand.SEND, "player", "userId", null).getMessageHeaders());
        sessionService.connect(validMessage);
        validMessage = MessageBuilder.createMessage(new ConnectionMessage("serviceSubscribeSession"),
                getAccessor(StompCommand.SUBSCRIBE, "player", "userId", "/topic/session/serviceSubscribeSession").getMessageHeaders());
        assertTrue(sessionService.validatePreSend(validMessage));

        // Пользователь не зарегистрирован, но пытается подписаться
        Message<ConnectionMessage> invalidMessage = MessageBuilder.createMessage(new ConnectionMessage("serviceSubscribeSession"),
                getAccessor(StompCommand.SUBSCRIBE, "nameDoesntExist", "", "/topic/session/serviceSubscribeSession").getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidMessage));


        invalidMessage = MessageBuilder.createMessage(new ConnectionMessage("serviceSubscribeSession"),
                getAccessor(StompCommand.SUBSCRIBE, "player", "anotherUserId", "/topic/session/serviceSubscribeSession").getMessageHeaders());
        assertFalse(sessionService.validatePreSend(invalidMessage));
    }

    @Test
    void handleWordTest() {
        // Зарегистрировать сессию
        Message<ConnectionMessage> validSend = MessageBuilder.createMessage(new ConnectionMessage("handleWordSession"),
                getAccessor(StompCommand.SEND, "player", "existingSession", null).getMessageHeaders());
        sessionService.connect(validSend);
        GameMessage gameMessage = new GameMessage(new Line(), null);
        WordMessageHandlingResult result = sessionService.handleWordMessage("handleWordSession", "player", gameMessage);
        assertNull(result.getGameMessage());
        assertNull(result.getMessageToGuesser());
        gameMessage = new GameMessage(null, "__12345UnexistingWord12345__");
        result = sessionService.handleWordMessage("unexistingHandleWordSession", "player", gameMessage);
        assertNull(result.getGameMessage());
        assertNull(result.getMessageToGuesser());
        result = sessionService.handleWordMessage("handleWordSession", "player", gameMessage);
        assertEquals(gameMessage, result.getGameMessage());
        assertNull(result.getMessageToGuesser());
    }

    @Test
    void handleImageTest() {
        // Зарегистрировать сессию
        Message<ConnectionMessage> validSend = MessageBuilder.createMessage(new ConnectionMessage("handleImageSession"),
                getAccessor(StompCommand.SEND, "player", "existingSession", null).getMessageHeaders());
        sessionService.connect(validSend);
        GameMessage gameMessage = new GameMessage(new Line(), null);
        assertNull(sessionService.handleImageMessage("unexistingHandleImageSession", "player", "existingSession", gameMessage));
        assertEquals(gameMessage, sessionService.handleImageMessage("handleImageSession", "player", "existingSession", gameMessage));
    }

    private StompHeaderAccessor getAccessor(StompCommand command, String destination) {
        return getAccessor(command, "login", "session", destination);
    }

    private StompHeaderAccessor getAccessor(StompCommand command, String login, String userSessionId) {
        return getAccessor(command, login, userSessionId, null);
    }

    private StompHeaderAccessor getAccessor(StompCommand command, String login, String userSessionId, String destination) {
        StompHeaderAccessor accessor = StompHeaderAccessor.create(command);
        accessor.setLogin(login);
        accessor.setSessionId(userSessionId);
        accessor.setDestination(destination);
        return accessor;
    }
}
