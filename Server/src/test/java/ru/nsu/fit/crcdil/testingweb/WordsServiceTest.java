package ru.nsu.fit.crcdil.testingweb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.nsu.fit.crcdil.service.WordsService;

import javax.sql.DataSource;

import static org.junit.Assert.*;


@ContextConfiguration
@SpringBootTest
class WordsServiceTest {
    private WordsService wordsService;


    @Autowired
    private DataSource dataSource;
    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @BeforeEach
    void setup() {
        wordsService = new WordsService(jdbcTemplate(dataSource));
    }

    @Test
    void getOtherWordTest() {
        // Проверка уникальности слов ()
        String lastWord = wordsService.getOtherWord();
        for(int i = 0; i < 100; ++i) {
            String newWord = wordsService.getOtherWord();
            assertNotEquals(lastWord, newWord);
            lastWord = newWord;
        }
    }

    @Test
    void isWordCorrectTest() {
        // Корректные сообщения
        wordsService.setFuzzyIsUsed(false);
        assertFalse(wordsService.isWordCorrect(""));
        String word = wordsService.getOtherWord();
        assertFalse(wordsService.isWordCorrect("a" + word));
        assertFalse(wordsService.isWordCorrect(word + "a"));
        for(int i = 0; i < 100; ++i) {
            assertTrue(wordsService.isWordCorrect(word));
            assertTrue(wordsService.isWordCorrect(word.toUpperCase()));
            assertTrue(wordsService.isWordCorrect(word.toLowerCase()));
            String newWord = wordsService.getOtherWord();
            assertFalse(wordsService.isWordCorrect(word));
            word = newWord;
        }
        wordsService.setFuzzyIsUsed(true);
        assertFalse(wordsService.isWordCorrect(""));
        word = wordsService.getOtherWord();
        assertTrue(wordsService.isWordCorrect("a" + word));
        assertTrue(wordsService.isWordCorrect(word + "a"));
        for(int i = 0; i < 100; ++i) {
            assertTrue(wordsService.isWordCorrect(word));
            assertTrue(wordsService.isWordCorrect(word.toUpperCase()));
            assertTrue(wordsService.isWordCorrect(word.toLowerCase()));
            String newWord = wordsService.getOtherWord();
            assertFalse(wordsService.isWordCorrect(word));
            word = newWord;
        }
    }

    @Test
    void getCurrentWordTest() {
        // Корректные сообщения
        String word = wordsService.getOtherWord();
        assertEquals(word, wordsService.getCurrentWord());
        assertTrue(wordsService.isWordCorrect(wordsService.getCurrentWord()));
    }
}


